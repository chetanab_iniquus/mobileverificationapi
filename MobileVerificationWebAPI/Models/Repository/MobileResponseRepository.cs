﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileVerificationWebAPI.Models.Repository
{
    public class MobileResponseRepository
    {
        public string ContactReference { get; set; }
        public string RepEmail { get; set; }
        public string RepName { get; set; }
    }
}