﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using MobileVerificationWebAPI.Models.MobileVerification;
using System.Net;
using System.IO;
using System.Configuration;
using MobileVerificationWebAPI.BuisinessLayer;


namespace MobileVerificationWebAPI.Models.Repository
{
    public class MobileRepository 
    {
        private readonly Repository _repository = new Repository();

        public JObject ValidateTwoFACode(string token, int twoFACode)
        {
            try
            {
                using (var dbConn = new DBModel())
                {
                    int contactid = dbConn.spMV_GetandSavetokenDetails(0, token, "view",false).FirstOrDefault().Value ;
                    var detailsofTwoFactorOTP = dbConn.spMV_ValidateTwoFACode(contactid, twoFACode).ToList();
                    return _repository.Response(detailsofTwoFactorOTP);
                }
            }
            catch (Exception ex)
            {
                return new JObject
                        {
                            {"statusCode", 503},
                            {"error", ex.Message},
                            {"data", null},
                            {"status", false}
                        };
            }
        }
    

    public JObject ValidateContactSecurityCheck(string dateofBirth,string  token)
        {
            try
            {
                using (var dbConn = new DBModel())
                {
                    int contactid = dbConn.spMV_GetandSavetokenDetails(0, token, "view", false).FirstOrDefault().Value;
                    var detailsofContactSecurity = dbConn.spMV_CheckContactSecurity(dateofBirth, contactid).ToList();
                    return _repository.Response(detailsofContactSecurity);
                }
            }
            catch (Exception ex)
            {
                return new JObject
                        {
                            {"statusCode", 503},
                            {"error", ex.Message},
                            {"data", null},
                            {"status", false}
                        };
            }
        }
        public JObject GetMobileCountryCodeDetails()
        {
            try
            {
                using (var dbConn = new DBModel())
                {
                    var detailsofMobileCountryCode = dbConn.spMV_GetMobileCountryCodeDetails().ToList(); ;
                    return _repository.Response(detailsofMobileCountryCode);
                }
            }
            catch (Exception ex)
            {
                return new JObject
                        {
                            {"statusCode", 503},
                            {"error", ex.Message},
                            {"data", null},
                            {"status", false}
                        };
            }
        }
        public JObject UpdateClientMobilenumber(JObject mobilerequestDetails)
        {
            try
            {
                MobileRequestRepository mobileRequestRepository = new MobileRequestRepository();
                mobileRequestRepository = _repository.MobileRequestRepository(mobilerequestDetails);
                using (var dbConn = new DBModel())
                {
                    int ContactID = dbConn.spMV_GetandSavetokenDetails(0, mobileRequestRepository.Token, "view", false).FirstOrDefault().Value;
                    var resultofUpdateMobileno = dbConn.spContacts_verifyMobileNo(ContactID,false, mobileRequestRepository.IsMobileLocked, mobileRequestRepository.Mode,
                        Convert.ToString(ContactID), mobileRequestRepository.NewMobileNo, "").FirstOrDefault();
       
                    if (resultofUpdateMobileno.Success == 1 && (!resultofUpdateMobileno.IsMobileVerified.Value))
                    {
                        dbConn.spMV_GetandSavetokenDetails(ContactID, mobileRequestRepository.Token, "save",true);
                        if (mobileRequestRepository.IsMobileLocked)
                        {
                            var reasultfromDB = dbConn.spContact_GetContactNameRef(ContactID).ToList();
                            if (reasultfromDB.Count > 0)
                            {
                                string MailBody;
                                StreamReader fileSR;
                                fileSR = File.OpenText(ConfigurationManager.AppSettings["TraderMobileNotificationMailer"]);
                                MailBody = fileSR.ReadToEnd();
                                fileSR.Close();
                                MailBody = MailBody.Replace("&lt;%ClientFullName%&gt;", reasultfromDB[0].FullName);
                                MailBody = MailBody.Replace("&lt;%ContactReference%&gt;", (reasultfromDB[0].ContactReference != "") ? "(" + reasultfromDB[0].ContactReference  + ")"  : "");


                                var emailToSendSmartTrader = new EmailMessageClass()
                                {
                                    BodyOfEmail = MailBody,
                                    FromEmail = string.IsNullOrEmpty(ConfigurationManager.AppSettings["FromEmail"]) ? "smartaccounts@smartcurrencyexchange.com" : ConfigurationManager.AppSettings["FromEmail"],
                                    //FromName = "SmartCurrencyExchange",
                                    ToEmail = ConfigurationManager.AppSettings["ComplianceMailID"],
                                    Subject = "Client has updated their mobile number and requires verification",
                                    //ToName = "SmartAccounts"
                                };
                                SendMailerBl.SendMailerEmail(emailToSendSmartTrader);
                            }
                        }
                    }
                    return _repository.Response(resultofUpdateMobileno);
                }
            }
            catch (Exception ex)
            {
                return new JObject
                        {
                            {"statusCode", 503},
                            {"error", ex.Message},
                            {"data", null},
                            {"status", false}
                        };
            }
        }
        public JObject GenerateandSendTwoFACode(string token,String mobileNumber)
        {
            try
            {
                Random rnd = new Random();
                int confirmCode = rnd.Next(100000, 999999);
                
                var URL = ConfigurationManager.AppSettings["TwoFAAPIURL"] + "mobileNumber=" + mobileNumber + "&code=" + Convert.ToString(confirmCode);
                var result = "";
                var request = WebRequest.Create(URL) as HttpWebRequest;
                //  Get response  
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    // Get the response stream  
                    var reader = new StreamReader(response.GetResponseStream());
                    // Read the whole contents and return as a string  
                    result = reader.ReadToEnd();
                    result = result.Substring(1, (result.Length - 2));
                   
                    if (result.Contains("success"))
                    {
                        using (var dbConn = new DBModel())
                        {
                            int contactId = dbConn.spMV_GetandSavetokenDetails(0, token, "view", false).FirstOrDefault().Value;

                            var returnTwoFAresultDB = dbConn.spContact_UpdateTwoFACode(contactId, Convert.ToString(confirmCode), contactId, "API").ToList(); ;
                            return _repository.Response(returnTwoFAresultDB);
                        }
                    }
                }
                return _repository.Response(new JObject { { "Status", 0 } });
            }
            catch (Exception ex)
            {
                return new JObject
                        {
                            {"statusCode", 503},
                            {"error", ex.Message},
                            {"data", null},
                            {"status", false}
                        };
            }
        }

        public JObject IsValidToken(string token)
        {
            try
            {
                bool Status = false;
                using (var dbConn = new DBModel())
                {
                    var reasultfromDB = dbConn.spMV_Validatetoken(token).FirstOrDefault();
                    if (reasultfromDB.Value == 1)
                        Status = true;
                }
                return _repository.Response(new JObject {{"tokenexpired",Status}});
            }
            catch (Exception ex)
            {
                return new JObject
                        {
                            {"statusCode", 503},
                            {"error", ex.Message},
                            {"data", null},
                            {"status", false}
                        };
            }

        }
    }
}