﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MobileVerificationWebAPI.Models.Repository
{
    public class MobileRequestRepository
    {
        [JsonProperty("Token")]
        public String Token { get; set; }
        [JsonProperty("IsMobileLocked")]
        public bool IsMobileLocked { get; set; }
        [JsonProperty("Mode")]
        public String Mode { get; set; }
        [JsonProperty("NewMobileNo")]
        public String NewMobileNo { get; set; }                 
    }
}