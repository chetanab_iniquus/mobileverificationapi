﻿using System.Collections;

namespace MobileVerificationWebAPI.Models.MobileVerification
{
    public class EmailMessageClass
    {
        public string FromName { get; set; }

        public string FromEmail { get; set; }

        public string ToName { get; set; }

        public string ToEmail { get; set; }

        public string Subject { get; set; }

        public string BodyOfEmail { get; set; }

        public string CC { get; set; }

        public string BCC { get; set; }

        public ArrayList AttachmentFiles { get; set; }
    }
}