﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.IO;
using MobileVerificationWebAPI.Models.MobileVerification;
using System.Text;

namespace MobileVerificationWebAPI.BuisinessLayer
{
    public class SendMailerBl
    {
        public static bool SendMailerEmail(EmailMessageClass messageToSend)
        {
            try
            {
                if (Convert.ToString(ConfigurationManager.AppSettings["IsSendGrid"]) == "true")
                {
                    string url = ConfigurationManager.AppSettings["SendgridWrapperAPI"];
                    HttpWebRequest http = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    http.Accept = "application/json";
                    http.ContentType = "application/json";
                    http.Method = "POST";
                    var strAttachmentFiles = new StringBuilder();
                    string strParsedContent = string.Empty;
                    string mailBody = string.Empty;
                    if (messageToSend.AttachmentFiles != null)
                    {
                        if (messageToSend.AttachmentFiles.Count > 0)
                        {
                            int i = 0;
                            var loopTo = messageToSend.AttachmentFiles.Count - 1;
                            for (i = 0; i <= loopTo; i++)
                            {
                                if (i == 0)
                                {
                                    strAttachmentFiles.Append("[ ");
                                }
                                strAttachmentFiles.Append((i > 0 ? "," : "") + "{\"filePath\"" + ": " + "\"" + Convert.ToString(messageToSend.AttachmentFiles[i]).Replace(@"\", "/") + "}");
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(strAttachmentFiles)))
                    {
                        strParsedContent = ", " + "\"attachmentFiles\"" + ": " + Convert.ToString(strAttachmentFiles) + "]";
                    }

                    if (ConfigurationManager.AppSettings["IsLive"] == "false")
                    {
                        messageToSend.BodyOfEmail = "From: " + messageToSend.FromEmail + " <br> To: " + messageToSend.ToEmail + " <br> CC: " + Convert.ToString(messageToSend.CC) + " <br> BCC: " + Convert.ToString(messageToSend.BCC) + "<br><br>" + messageToSend.BodyOfEmail;
                        messageToSend.ToEmail = ConfigurationManager.AppSettings["mailtoSmartArchive"];
                        messageToSend.CC = "";
                        messageToSend.BCC = "";
                    }
                    mailBody = messageToSend.BodyOfEmail;
                    // 'Replace below character in filecontent for parsing to send mail
                    //mailBody = mailBody.Replace(Environment.NewLine, string.Empty);
                    mailBody = mailBody.Replace(@"\", "/");
                    mailBody = mailBody.Replace("\"", @"\""");
                    string parsedContent = "{" + "\"fromEmail\"" + ": " + "\"" + messageToSend.FromEmail + "\"" + ", " + "\"fromName\"" + ": " + "\"" + messageToSend.FromName + "\"" + ", " + "\"toName\"" + ": " + "\"" + messageToSend.ToName + "\"" + ", " + "\"toEmail\"" + ": " + "\"" + messageToSend.ToEmail + "\"" + ", " + "\"subject\"" + ": " + "\"" + messageToSend.Subject + "\"" + ", " + "\"mailBody\"" + ": " + "\"" + mailBody + "\"" + ", " + "\"ccEmail\"" + ": " + "\"" + messageToSend.CC + "\"" + ", " + "\"bccEmail\"" + ": " + "\"" + messageToSend.BCC + "\"" + strParsedContent + "}";
                    var bytes = Encoding.UTF8.GetBytes(parsedContent);
                    //var encoding = new ASCIIEncoding();
                    //var bytes = encoding.GetBytes(parsedContent);
                    Stream newStream = http.GetRequestStream();
                    newStream.Write(bytes, 0, bytes.Length);
                    newStream.Close();
                    using (HttpWebResponse response = http.GetResponse() as HttpWebResponse)
                    {
                        // Get the response stream  
                        var reader = new StreamReader(response.GetResponseStream());
                        string result = "";
                        result = reader.ReadToEnd();
                        string str = "Successfully Executed";
                        if (result.IndexOf(str) > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    var smtpClient = new SmtpClient();
                    using (var mailMessage = new MailMessage())
                    {
                        var fromMailAddress = new MailAddress(messageToSend.FromEmail, messageToSend.FromName);
                        var toMailId = string.Empty;
                        var ccMailId = string.Empty;
                        var bccMailId = string.Empty;

                        if (ConfigurationManager.AppSettings["IsLive"] == "false")
                        {
                            toMailId = ConfigurationManager.AppSettings["mailtoSmartArchive"];
                            MailAddress toMailAddress = new MailAddress(toMailId);
                            mailMessage.To.Add(toMailAddress);
                            messageToSend.BodyOfEmail = "From: " + messageToSend.FromEmail + " <br> To: " + messageToSend.ToEmail + " <br> CC: " + Convert.ToString(messageToSend.CC) + " <br> BCC: " + Convert.ToString(messageToSend.BCC) + "<br><br>" + messageToSend.BodyOfEmail;
                        }
                        else
                        {
                            toMailId = messageToSend.ToEmail;
                            if (messageToSend.CC != null)
                                ccMailId = messageToSend.CC;
                            if (messageToSend.BCC != null)
                                bccMailId = messageToSend.BCC;

                            string[] Tos = toMailId.Split(';');
                            int i = 0;
                            for (i = 0; i <= Tos.Length - 1; i++)
                            {
                                if (Tos[i] != "")
                                {
                                    MailAddress toMailAddress = new MailAddress(Convert.ToString(Tos[i]));
                                    mailMessage.To.Add(toMailAddress);
                                }
                            }

                            string[] CCs = ccMailId.Split(';');
                            int j = 0;
                            for (j = 0; j <= CCs.Length - 1; j++)
                            {
                                if (CCs[j] != "")
                                {
                                    MailAddress ccMailAddress = new MailAddress(Convert.ToString(CCs[j]));
                                    mailMessage.CC.Add(ccMailAddress);
                                }
                            }

                            string[] BCCs = bccMailId.Split(';');
                            int k = 0;
                            for (k = 0; k <= BCCs.Length - 1; k++)
                            {
                                if (BCCs[k] != "")
                                {
                                    MailAddress bccMailAddress = new MailAddress(Convert.ToString(BCCs[k]));
                                    mailMessage.Bcc.Add(bccMailAddress);
                                }
                            }
                        }

                        smtpClient.Credentials = new NetworkCredential(
                            ConfigurationManager.AppSettings["nwUsername"],
                            ConfigurationManager.AppSettings["nwPassword"]
                        );
                        smtpClient.Port = System.Convert.ToInt32(ConfigurationManager.AppSettings["nwPort"]);
                        smtpClient.Host = ConfigurationManager.AppSettings["nwSMTP"];

                        mailMessage.From = fromMailAddress;
                        mailMessage.Subject = messageToSend.Subject;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = messageToSend.BodyOfEmail;

                        int iCnt;
                        if (messageToSend.AttachmentFiles != null)
                        {
                            iCnt = messageToSend.AttachmentFiles.Count - 1;
                            for (int i = 0, loopTo = iCnt; i <= loopTo; i++)
                            {
                                string[] strTemp;
                                strTemp = Convert.ToString(messageToSend.AttachmentFiles[i]).Split('#');
                                if (File.Exists(strTemp[0]))
                                {
                                    string strfile = strTemp[0].ToString().Replace("/", @"\");
                                    var objAttach = new Attachment(strfile);
                                    if (strTemp.Length > 1)
                                    {
                                        if (!string.IsNullOrEmpty(strTemp[1]))
                                        {
                                            objAttach.Name = strTemp[1];
                                        }
                                    }
                                    mailMessage.Attachments.Add(objAttach);
                                }
                            }
                        }

                        smtpClient.EnableSsl = true;

                        try
                        {
                            smtpClient.Send(mailMessage);
                        }
                        catch (AuthenticationException e)
                        {
                            if (e.InnerException is Win32Exception)
                                smtpClient.Send(mailMessage);
                            else
                                throw;
                        }
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //Send Email for Exception
                EmailMessageClass emailToSend = new EmailMessageClass()
                {
                    BodyOfEmail = ex.ToString(),
                    FromEmail = ConfigurationManager.AppSettings["ErrorMail_From"],
                    FromName = ConfigurationManager.AppSettings["FromName"],
                    ToEmail = ConfigurationManager.AppSettings["ErrorMail_Recipient"].ToString(),
                    Subject = "Error from Mobile Verification API on SendMailerEmail",
                    ToName = ConfigurationManager.AppSettings["ToName"]
                };

                SendMailerEmail(emailToSend);
            }
            return false;
        }
    }
}