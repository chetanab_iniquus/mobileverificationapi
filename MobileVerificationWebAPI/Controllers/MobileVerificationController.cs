﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileVerificationWebAPI.Models.Repository;
using Newtonsoft.Json.Linq;
using System.Web.Http.Cors;

namespace MobileVerificationWebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api")]
    public class MobileVerificationController : ApiController
    {
        private readonly MobileRepository _mobileRepository = new MobileRepository();

        [HttpGet]
        [Route("autheticateToken")]
        public IHttpActionResult ValidateToken(string token)
        {
            var responseJson = _mobileRepository.IsValidToken(token);
            return Content(HttpStatusCode.OK, responseJson);
        }

        [HttpGet]
        [Route("ValidateTwoFACode")]
        public IHttpActionResult ValidateTwoFACode(string token, int twoFACode)
        {
            var responseJson = _mobileRepository.ValidateTwoFACode(token, twoFACode);
            return Content(HttpStatusCode.OK, responseJson);
        }

        [HttpGet]
        [Route("get")]
        public IEnumerable<string> Get()
        {
            return new string[] { "Apple", "Grape", "Kiwi", "Mango" };
        }

        [HttpGet]
        [Route("ContactSecurityCheck")]
        public IHttpActionResult ValidateContactSecurityCheck(string token,string dateofBirth)
        {
            var responseJson = _mobileRepository.ValidateContactSecurityCheck(dateofBirth, token);
            return Content(HttpStatusCode.OK, responseJson);
        }

        [HttpGet]
        [Route("MobileCountryCodeDetails")]
        public IHttpActionResult GetMobileCountryCodeDetails()
        {
            var responseJson = _mobileRepository.GetMobileCountryCodeDetails();
            return Content(HttpStatusCode.OK, responseJson);
        }

        [HttpPost]
        [Route("UpdateClientMobileno")]
        public IHttpActionResult UpdateClientMobilenumber(JObject mobilerequestDetails)
        {
            var responseJson = _mobileRepository.UpdateClientMobilenumber(mobilerequestDetails);
            return Content(HttpStatusCode.OK, responseJson);
        }

        [HttpGet]
        [Route("GenerateTwoFACode")]
        public IHttpActionResult GenerateandSendTwoFACode(string token, String mobileNumber)
        {
            var responseJson = _mobileRepository.GenerateandSendTwoFACode(token, mobileNumber);
            return Content(HttpStatusCode.OK, responseJson);
        }
    }

}
